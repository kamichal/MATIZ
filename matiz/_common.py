import dataclasses
import logging

import yaml

logger = logging.getLogger(__name__)


def log(message):
    """Dummy _logger, meets mvp."""
    print(message)


def load_y(yaml_string: str) -> object:
    """Shortcut to yaml.dump (since explicit Loader specification became required)."""
    return yaml.load(yaml_string, Loader=yaml.Loader)


def dump_y(py_object: object) -> str:
    """Shortcut to yaml.dump (since explicit Dumper specification became required)."""
    return yaml.dump(py_object, Dumper=yaml.Dumper)


def is_dataclass_instance(obj):
    return hasattr(obj, '__dataclass_params__')


def fields_n_values(object_):
    if is_dataclass_instance(object_):
        yield from zip(dataclasses.fields(object_), dataclasses.astuple(object_))
    else:
        logger.error(f"Cannot use {type(object_).__name__} as a dataclass.")
