# pylint: disable=not-callable

import dataclasses
import functools
import logging
import typing

from . import _valid

MISSING = dataclasses.MISSING
MissingOrAny = typing.Union[type(MISSING), typing.Any]
_logger = logging.getLogger(__name__)


class IzFieldBase(dataclasses.Field):
    """Specifies details of own dataclasses.Field object."""
    iz_validators = None

    def __init__(
            self,
            mandatory: bool = False,
            description: typing.Optional[str] = None,

            # use `default` for scalars (or any immutable type)
            default: MissingOrAny = MISSING,
            # or `default_factory` for mutable kinds, like collections.
            # It has to be callable being a default constructor in C++' meaning.
            default_factory: MissingOrAny = MISSING,

            # use those below to tell dataclasses if this field has to be used in generation
            # of corresponding methods of parent container (refer to `dataclass.field()` docs).
            init: bool = True,
            repr_: bool = True,
            hash_: bool = None,
            compare: bool = True,
            metadata: typing.Any = None,
    ):
        self.mandatory = mandatory
        self.description = description

        if default is not MISSING and default_factory is not MISSING:
            raise ValueError('Cannot specify both default and default_factory.')
        super().__init__(default, default_factory, init, repr_, hash_, compare, metadata)

    def get_default(self):
        if self.default_factory is not MISSING:
            try:
                return self.default_factory()
            except (TypeError, ValueError, AttributeError) as e:
                msg = f"default_factory failed: {type(e).__name__}: {e}."
                _logger.error(msg)
                return self.default_factory

        if self.default is not MISSING:
            return self.default
        msg = f"Unable to get_default() for field {type(self).__name__}.{self.name}."
        if not self.default_factory:
            msg += f" Neither `default` nor `default_factory` attribute defined."
        raise TypeError(msg)

    def violations(self, py_value, path_) -> typing.Generator:
        if self.mandatory and py_value is MISSING:
            yield _valid.Violation(path_, "Mandatory value is missing.")

    def __repr__(self):
        return f"{self.__class__.__name__}(...)"


class TObject(IzFieldBase):

    def __init__(self,
                 default_constructor: typing.Callable,
                 *,
                 mandatory: bool = False,
                 description: typing.Optional[str] = None,
                 default: MissingOrAny = _valid.MISSING,
                 init: bool = True,
                 repr_: bool = True, hash_: bool = None, compare: bool = True, metadata: typing.Any = None):
        super().__init__(mandatory, description, default, default_constructor, init, repr_, hash_, compare, metadata)


class TList(IzFieldBase):

    def __init__(self,
                 item_default_constructor: IzFieldBase,
                 *,
                 min_len=None,
                 max_len=None,
                 mandatory: bool = False,
                 description: typing.Optional[str] = None,
                 default: MissingOrAny = _valid.MISSING,
                 init: bool = True,
                 repr_: bool = True, hash_: bool = None, compare: bool = True, metadata: typing.Any = None):
        super().__init__(mandatory, description, default, item_default_constructor, init, repr_, hash_, compare,
                         metadata)
        self.item_default_constructor = item_default_constructor

    def violations(self, py_value, path) -> typing.Generator[_valid.Violation, None, None]:
        yield from super().violations(py_value, path)
        for index, item in enumerate(py_value):
            print(f"self.default_factory: {self.item_default_constructor}")
            if hasattr(self.item_default_constructor, 'violations'):
                yield from self.item_default_constructor.violations(item, f"{path}[{index}]")
            else:
                print("Cannot validate")


class TString(IzFieldBase):
    default = ''

    def __init__(self, min_len=None, max_len=None, **other_args):
        self.length_range = _valid.RangeValidator(min_len, max_len)
        if 'default' not in other_args:
            other_args['default'] = self.__class__.default

        super().__init__(**other_args)

    def violations(self, py_value, path) -> typing.Generator:
        yield from super().violations(py_value, path)
        yield from self.length_range.validate(len(str(py_value)), path)


class TInt(IzFieldBase):
    minimum = None
    maximum = None
    signed = False
    default = 0

    def __init__(self, minimum=None, maximum=None, **other_args):
        if minimum is None:
            minimum = self.minimum
        if maximum is None:
            maximum = self.maximum
        if 'default' not in other_args:
            other_args['default'] = self.__class__.default

        self.value_range = _valid.RangeValidator(minimum, maximum)
        super().__init__(**other_args)

    def violations(self, py_value, path) -> typing.Generator[_valid.Violation, None, None]:
        print(f"checking violations {self.__class__.__name__}: {path}")
        yield from super().violations(py_value, path)
        yield from self.value_range.validate(py_value, path)


def _make_int_type(signed, bits_length):
    bytes_num, reminder = divmod(bits_length, 8)
    assert not reminder, "Number of bits has to be a multiply of 8"
    full = 2 ** bits_length

    def wrapper(cls):
        if signed:
            cls.minimum = -1 * full / 2
            cls.maximum = -1 + full / 2
        else:
            cls.minimum = 0
            cls.maximum = - 1 + full

        return cls

    return wrapper


_unsigned = functools.partial(_make_int_type, False)
_signed = functools.partial(_make_int_type, True)


@_unsigned(8)
class TUInt8(TInt):
    pass


@_unsigned(16)
class TUInt16(TInt):
    pass


@_unsigned(32)
class TUInt32(TInt):
    pass


@_unsigned(64)
class TUInt64(TInt):
    pass


@_signed(8)
class TInt8(TInt):
    pass


@_signed(16)
class TInt16(TInt):
    pass


@_signed(32)
class TInt32(TInt):
    pass


@_signed(64)
class TInt64(TInt):
    pass
