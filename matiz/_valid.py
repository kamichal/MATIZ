import dataclasses
import logging
import typing

from . import _common

_logger = logging.getLogger(__name__)

MISSING = dataclasses.MISSING
MissingOrAny = typing.Union[type(MISSING), typing.Any]


class Validable:

    @staticmethod
    def is_validable(py_object):
        return _common.is_dataclass_instance(py_object) and isinstance(py_object, Validable)

    def is_valid(self):
        return not list(self.violations())

    def violations(self, path=None):
        if self.is_validable(self):
            path = path or self.__class__.__name__
            for field_object, py_value in _common.fields_n_values(self):
                yield from field_object.violations(py_value, f"{path}.{field_object.name}")
        else:
            _logger.warning(f"Attempted to validate incompatible object type: {self.__class__.__name__}.")


@dataclasses.dataclass
class Violation:
    path_: str
    message: str

    def __str__(self):
        return f"{self.path_ or '<no path>'} | {self.message or '<no message given>'}"


@dataclasses.dataclass
class RangeValidator:
    minimum: int
    maximum: int

    def validate(self, value, path_):
        if self.minimum is not None and self.maximum is not None:
            assert self.minimum <= self.maximum, "Sick range definition. Min lower than max."

        if value is None:
            yield Violation(path_, f"Value cannot be None.")
            return
        if self.minimum is not None and value < self.minimum:
            yield Violation(path_, f"Value: {value!r} exceeds minimum: {self.minimum!r}.")
        if self.maximum is not None and value > self.maximum:
            yield Violation(path_, f"Value: {value!r} exceeds maximum: {self.maximum!r}.")
