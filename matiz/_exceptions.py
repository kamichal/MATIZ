class BadYamlizDefinition(TypeError):
    pass


class UnYamlizable(TypeError):
    pass


class UnPythonizable(TypeError):
    pass
