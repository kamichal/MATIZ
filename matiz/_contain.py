import dataclasses
import logging
from itertools import chain

_logger = logging.getLogger(__name__)


class DataclassFieldMeta(type):

    def __init__(cls, name, bases, attrs):
        """Almost the same is done by yaml.YAMLObjectMetaclass"""

        if '__dataclass_params__' not in attrs:
            msg = f"Matiz class {name!r} must be a dataclass. " \
                  f"To automatically decorate all the derived classes - replace " \
                  f"this log call with following statement: " \
                  f"`dataclasses.dataclass(cls)`."
            _logger.error(msg)
            print(f"\nError: Not a dataclass. {msg}")
        super(DataclassFieldMeta, cls).__init__(name, bases, attrs)

    def unused__call__(cls, *unknown_args, **unknown_kwargs):
        native_args = []
        native_kwargs = {}
        positional_fields_consumed = set()
        unknown_args = list(unknown_args)

        _trace_call(cls, unknown_args, unknown_kwargs, f"\nclass {cls.__name__}:\noriginal")

        for i, field in enumerate(dataclasses.fields(cls)):
            if field.name in {'aux_call_args', 'aux_call_kwargs'}:
                continue
            if i < len(unknown_args):
                native_args.append(unknown_args.pop(0))
                positional_fields_consumed.add(field.name)
            elif field.name in unknown_kwargs:
                duplication_msg = f"Duplicated values for argument {field.name}."
                assert field.name not in positional_fields_consumed, duplication_msg
                assert field.name not in native_kwargs, duplication_msg
                assert field.name != 'aux_call_kwargs', f"Bad call-arguments propagation in {cls.__name__}."
                native_kwargs[field.name] = unknown_kwargs.pop(field.name)
            else:
                native_kwargs[field.name] = field.get_default()

        # be careful for api delivered by dataclasses __init__

        native_kwargs['aux_call_args'] = unknown_args
        native_kwargs['aux_call_kwargs'] = unknown_kwargs
        _trace_call(cls, unknown_args, unknown_kwargs, "native")
        instance = super().__call__(*native_args, **native_kwargs)

        return instance


def _trace_call(cls, positional_args, keyword_args, msg=""):
    statement = _form_call_statement(cls, positional_args, keyword_args)
    _logger.debug(f" {msg}: calling: {statement}")
    print(f" {msg}: calling: {statement}")


def _form_call_statement(cls, positional_args, keyword_args):
    args_ = (str(repr(a)) for a in positional_args)
    kwargs_ = (f"{k}={v!r}" for k, v in keyword_args.items())
    return f"{cls.__name__}({', '.join(chain(args_, kwargs_))})"


class Containable(metaclass=DataclassFieldMeta):
    """Intended to represent mapping type container.

    key is a valid python identifier
    value is derived from IzFieldBase
    """

    def as_dict(self):
        fields_names = {f.name for f in dataclasses.fields(self)}
        return {k: v for k, v in self.__dict__ if k in fields_names}

    @classmethod
    def from_dict(cls, arbitrary_kwargs):
        assert isinstance(arbitrary_kwargs, dict), f"Bad type: {type(arbitrary_kwargs)}."
        native_kwargs = {}
        for f in dataclasses.fields(cls):
            if f.name in arbitrary_kwargs:
                native_kwargs[f.name] = arbitrary_kwargs.pop(f.name)
        object_ = cls(**native_kwargs)
        object_.aux_call_kwargs = arbitrary_kwargs
        return object_

    @classmethod
    def from_list(cls, arbitrary_kwargs):
        assert isinstance(arbitrary_kwargs, dict), f"Bad type: {type(arbitrary_kwargs)}."
        native_kwargs = {}
        for f in dataclasses.fields(cls):
            if f.name in arbitrary_kwargs:
                native_kwargs[f.name] = arbitrary_kwargs.pop(f.name)
        object_ = cls(**native_kwargs)
        object_.aux_call_kwargs = arbitrary_kwargs
        return object_
