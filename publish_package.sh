#!/usr/bin/env bash
credentials_file=$HOME/.pypirc
venv_py_publishin_dir=$HOME/.venv_py_publish
releases_dir="$venv_py_publishin_dir/releases"

if [ "$1" == "clean" ]; then
  rm -rf "$releases_dir"
  echo "clean done on $releases_dir"
  exit 0
fi

package_root_dir=$1

if [ ! -d "$package_root_dir" ]; then
  echo "Call pointing to a existing directory."
  exit 23
fi

if [ ! -f "$package_root_dir/setup.py" ]; then
  echo "# Call pointing to a directory containing setup.py file, e.g:."
  echo "$0 path/to/package/root"
  exit 24
fi

if [ -f "$credentials_file" ]; then
  echo " # output of: $0"
  echo " # Pypi's credentials for package's upload should be placed in "
  echo " #  $credentials_file"
  echo " #"
  echo " # But are not."
  echo " # Following hints from:"
  echo " # https://truveris.github.io/articles/configuring-pypirc/"
  echo " # Please fill it with such kind of content:"
  echo ""
  echo "[distutils]"
  echo "index-servers="
  echo "    pypi"
  echo "    testpypi"
  echo ""
  echo "[pypi]"
  echo "username: brodan"
  echo "password: xxxxxxxxxxxxxxxx"
  echo ""
else
  echo "# credentials from $credentials_file are to be used for pypi"
fi

releases_dir="$venv_py_publishin_dir/releases"
venv_bin="$venv_py_publishin_dir"/bin
py_exec="$venv_py_publishin_dir"/bin/python

set -xe

if [ ! -d "$venv_py_publishin_dir" ] || [ ! -f "$py_exec" ]; then
  echo "# python interpreter not found in:"
  echo "# $py_exec"
  echo "# creating new virtualenv"
  virtualenv -p python3 "$venv_py_publishin_dir"
  "$venv_bin"/pip install -U pip setuptools twine check-manifest wheel
fi

mkdir -p releases_dir

cd "$package_root_dir"
"$venv_bin"/check-manifest
"$py_exec" setup.py check --strict --metadata
"$py_exec" setup.py clean
"$py_exec" setup.py sdist --dist-dir "$releases_dir"
"$py_exec" setup.py bdist_wheel --universal --dist-dir "$releases_dir"
"$py_exec" -m twine check "$releases_dir"*/*
"$venv_bin"/pip check "$releases_dir"
echo -e "\n # OK all checks done\n"
"$venv_bin"/twine upload --skip-existing "$releases_dir"/*
cd -
